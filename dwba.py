#! /usr/bin/env python

import numpy as np
from reflectivity import propagation_coeffs
import formfactor
import matplotlib.pyplot as plt

class SimGISXAS:
    def __init__ (self, qy, qz, wavelen, refindx):
        self.qy = qy
        self.qz = qz
        self.k0 = 2. * np.pi / wavelen
        self.alpha = None
        self.delta = refindx[0]
        self.beta = refindx[1]

    def calc_alpha(self, alphai):
        self.alpha = np.arcsin(self.qz/self.k0 - np.sin(alphai))

    def distortQz(self, alphai):
        if self.alpha is None: self.calc_alpha(alphai)
        ki = self.k0 * np.sin(alphai)
        kf = self.k0 * np.sin(self.alpha)
        return (kf+ki, kf-ki, -kf+ki, -kf-ki)

    def calcFresnelCoeffs(self, alpha, alphai):
        multilayer = MultiLayer()
        fc = multilayer.propagation_coeffs(alphai, alpha, self.k0, 0)
        return fc

    def run(self, params, alphai):
        self.calc_alpha(alphai)
        qz = self.distortQz(alphai)
        fc = propagation_coeffs(alphai, self.alpha, self.delta, self.beta)
        ff = np.zeros(self.qz.shape, dtype=np.complex_)

        # shape parameters
        y1 = params['left_coord']
        y2 = params['right_coord']
        dh = params['delta_h']
        la = params['langle']
        for i in range(4):
            ff +=  fc[i,:] * stacked_trapezoids(self.qy, qz[i], y1, y2, dh, la) 
        return ff

if __name__ == '__main__':

    # parameters
    NZ = 1024
    params = { 'left_coord':-25, 'right_coord':25, 'delta_h':10 }
    params['langle'] = np.deg2rad([90., 80, 80, 75, 70])
    alphai = np.deg2rad(0.15)
    delta = 4.88E-06
    beta = 7.37E-08
    qz = np.linspace(0, 2, NZ)
    qp = 0.125 * np.ones(qz.shape)
    wavelen = 0.123984



    sim = SimGISXAS(qp, qz, wavelen, (delta, beta))
    ff = sim.run(params, alphai) 
    ii = np.abs(ff)**2
     
    plt.semilogy(qz, ii)
    plt.show()
