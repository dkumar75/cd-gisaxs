#! /usr/bin/env python

import math 
import arrayfire as af
import numpy as np
import ipdb

class Layer:
    def __init__(self, delta, beta, order, thickness):
        self.one_n2 = 2 * complex(delta, beta)
        self.order = order
        self.thickness = thickness
        self.zval = 0

class MultiLayer:
    def __init__(self, substrate=(4.88E-06, 7.37E-08)):
        self.layers = [Layer(0, 0, 0, 0)]
        self.substrate = Layer(*substrate, -1, 0)
        self._setup_ = False

    def insert(self, layer):
        if not isinstance(layer, Layer):
            raise TypeError('only Layer types can be inserted into multilayered object')
            exit(101)
        if not layer.order > 0:
            raise ValueError('the order of layer must be greater than 0');
            exit(102)
        self.layer.insert(layer.order, layer)
    
    def setup_multilayer(self):
        if self._setup_ : return

        # put substrate at the end
        self.layers.append(self.substrate)

        # calc z of every interface
        nlayer = len(self.layers)
        for i in range(nlayer-2, -1, -1):
            self.layers[i].zval = self.layers[i+1].thickness
        
        # run only once
        self._setup_ = True

    def parratt_recursion(self, alpha, k0, order):
        self.setup_multilayer()

        nlayer = len(self.layers)
        # account for scalar case
        if isinstance(alpha, af.Array):
            shape = alpha.shape
            alpha = af.flat(alpha)
        elif isinstance(alpha, float):
            alpha = af.Array([alpha], (1,))
            shape = (1,)
        else:
            raise ValueError('Unknown/Unsupported format for alpha')

        # precompute sin
        sin_a = af.sin(alpha)

        # initialize
        dims = (alpha.elements(), nlayer)
        R = af.constant(0, *dims, dtype=af.Dtype.c32)
        T = af.constant(0, *dims, dtype=af.Dtype.c32)
        T[:,-1] = 1

        # cacl k-value
        kz = af.constant(0, *dims, dtype=af.Dtype.c32)
        for i in range(nlayer):
            kz[:,i] =  -k0 * af.sqrt(sin_a**2 - self.layers[i].one_n2)

        # iterate from bottom to top
        for i in range(nlayer-2, -1, -1):
            pij = (kz[:,i] + kz[:,i+1])/(2.* kz[:,i])
            mij = (kz[:,i] - kz[:,i+1])/(2.* kz[:,i])

            # remove ivalids
            pij[af.isinf(pij)] = 0
            mij[af.isinf(mij)] = 0

            z = self.layers[i].zval
            exp_p = af.exp(-1j*(kz[:,i+1]+kz[:,i])*z)
            exp_m = af.exp(-1j*(kz[:,i+1]-kz[:,i])*z)
            a00 = pij * exp_m
            a01 = mij * af.conjg(exp_p)
            a10 = mij * exp_p
            a11 = pij * af.conjg(exp_m)
            T[:,i] = a00 * T[:,i+1] + a01 * R[:,i+1]
            R[:,i] = a10 * T[:,i+1] + a11 * R[:,i+1]

        # normalize
        t0 = T[:,0].copy()
        t0[t0==0] = 1.
        for i in range(nlayer):
            T[:,i] /= t0
            R[:,i] /= t0

        T = af.moddims(T[:,order], *shape)
        R = af.moddims(R[:,order], *shape)
        return T, R

    def propagation_coeffs(self, alphai, alpha, k0, order):
        """
        Calulates propagation coefficents for a multilayerd structred,
        using the Parratt recursion.

        Parameters:
        -----------
        alphai: scalar or af.Array,radians, shape(n_ai,),
                incidence angle for GISAXS mesurement
        alpha: af.Array, radians
                exit angles from scattering events, i.e. sample to pixels
                alpha.shape == (N,) if alphai is a scalar
                alpha.shape == (N, n_ai) if alphai is a sequence
        k0: float, 1/nm
                2\u03C0/\u03BB
        order: int,
            index of the current layer

        Returns:
        fc: af.Array, shape == alpha.shape
                Propagation coefficients T's and R's 
        """
        if isinstance(alphai, af.Array):
            s1 = alphai.shape
        else:
            s1 = (1,)

        s2 = alpha.shape
        if len(s2) == 2 and s2[1] != s1[0]:
            raise ValueError('rdim of alpha != ldim of alpha_i')

        Ti, Ri = self.parratt_recursion(alphai, k0, order)
        Tf, Rf = self.parratt_recursion(alpha, k0, order)
        fc = af.constant(0, s2[0], s1[0], 4, dtype=af.Dtype.c32)

        # computer Fresnel coefficents
        fc[:,:,0] = af.tile(Ti.T, s2[0]) * af.conjg(Tf)
        fc[:,:,1] = af.tile(Ri.T, s2[0]) * af.conjg(Tf)
        fc[:,:,2] = af.tile(Ti.T, s2[0]) * af.conjg(Rf)
        fc[:,:,3] = af.tile(Ri.T, s2[0]) * af.conjg(Rf)
        fc[alpha < 0] = 0
        return fc
        
if __name__ == '__main__':
    nz = 600
    alphai = np.deg2rad(0.3)
    alpha = np.linspace(-alphai, 0.0145, nz)
    al = af.np_to_af_array(alpha)
    delta = 4.88E-06 
    beta =  7.37E-08 
    
    system = MultiLayer()
    k0 = 2 * math.pi / 0.123984
    fc = system.propagation_coeffs(alphai, al, k0, 0)
    fc = fc.__array__()

    import matplotlib.pyplot as plt
    plt.subplot(221)
    plt.plot(alpha, np.abs(fc[:,:,0]))
    plt.subplot(222)
    plt.plot(alpha, np.abs(fc[:,:,1]))
    plt.subplot(223)
    plt.plot(alpha, np.abs(fc[:,:,2]))
    plt.subplot(224)
    plt.plot(alpha, np.abs(fc[:,:,3]))
    plt.show()
