#! /usr/bin/env python

import numpy as np
import arrayfire as af
from gFresnel import MultiLayer
from gformfactor import stacked_trapezoids
import matplotlib.pyplot as plt
import ipdb

class SimGISXAS:
    def __init__ (self, qy, qz, wavelen, refindx):
        self.qy = qy
        self.qz = qz
        self.k0 = 2. * np.pi / wavelen
        self.alpha = None
        self.delta = refindx[0]
        self.beta = refindx[1]

    def calc_alpha(self, alphai):
        if self.alpha is None:
            n = self.qz.shape[0]
            self.alpha = af.asin(self.qz/self.k0 - \
                af.tile(af.sin(alphai.T), n))

    def distortQz(self, alphai):
        n = self.alpha.shape[0]
        ki = af.tile(-self.k0 * af.sin(alphai.T), n)
        kf = self.qz + ki
        return (kf-ki, kf+ki, -kf-ki, -kf+ki)

    def calcFresnelCoeffs(self, alpha, alphai):
        multilayer = MultiLayer()
        return multilayer.propagation_coeffs(alphai, alpha, self.k0, 0)
        

    def run(self, params, alphai):
        self.calc_alpha(alphai)
        qz = self.distortQz(alphai)
        fc = self.calcFresnelCoeffs(self.alpha, alphai)
        ff = af.constant(0, *qz[0].shape, dtype=af.Dtype.c32)

        # shape parameters
        y1 = params['left_coord']
        y2 = params['right_coord']
        dh = params['delta_h']
        la = params['langle']

        for i in range(4):
            ftemp = stacked_trapezoids(self.qy, qz[i], y1, y2, dh, la) 
            ff += fc[:,:,i] * ftemp
        return ff

if __name__ == '__main__':

    # parameters
    nz = 2000
    n_ai = 10
    params = { 'left_coord':-25, 'right_coord':25, 'delta_h':50 }
    params['langle'] = np.deg2rad(np.repeat(85, 1))
    alpha = np.deg2rad(np.linspace(0.0, 5.0, nz))
    alphai = np.deg2rad(np.linspace(0.14, 0.3, n_ai))
    delta = 4.88E-06
    beta = 7.37E-08
    wavelen = 0.123984

    import time
    t0 = time.time()
    ao = af.np_to_af_array(alpha)
    ai = af.np_to_af_array(alphai)
    qz = (2 * np.pi / wavelen) * (af.tile(af.sin(ao), 1, n_ai)  + \
            af.tile(af.sin(ai.T), nz))
    qy = af.constant(0.125, *qz.shape)
    sim = SimGISXAS(qy, qz, wavelen, (delta, beta))
    ff = sim.run(params, ai) 
    dt = time.time() - t0
    print('Time taken  = %f' % dt)

    ff = ff.__array__()
    ff = np.abs(ff)**2
    plt.semilogy(alpha, ff)
    plt.show()
