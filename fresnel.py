#! /usr/bin/env python

import numpy as np
import ipdb

class Layer:
    def __init__(self, delta, beta, order, thickness):
        self.one_n2 = 2 * np.complex(delta, beta)
        self.order = order
        self.thickness = thickness
        self.zval = 0

class MultiLayer:
    def __init__(self):
        self.layers = [Layer(0, 0, 0, 0)]
        self.substrate = Layer(4.88E-6, 7.37E-08, -1, 0)
        self._setup_ = False

    def insert(self, layer):
        if not isinstance(layer, Layer):
            raise TypeError('only Layer types can be inserted into multilayered object')
            exit(101)
        if not layer.order > 0:
            raise ValueError('the order of layer must be greater than 0');
            exit(102)
        self.layer.insert(layer.order, layer)
    
    def setup_multilayer(self):
        if self._setup_ : return

        # put substrate at the end
        self.layers.append(self.substrate)

        # calc z of every interface
        nlayer = len(self.layers)
        for i in range(nlayer-2, -1, -1):
            self.layers[i].zval = self.layers[i+1].thickness
        
        # run only once
        self._setup_ = True

    def parratt_recursion(self, alpha, k0, order):
        self.setup_multilayer()
        nlayer = len(self.layers)
        shape = np.shape(alpha)
        # account for scalar case
        if len(shape) == 0:
            shape = (1,)

        # sin(alpha)
        sin_a = np.sin(alpha)

        # initialize
        dims = (nlayer,) + shape
        R = np.zeros(dims, np.complex_)
        T = np.zeros(dims, np.complex_)
        T[-1,:] = np.complex(1, 0)

        # cacl k-value
        kz = np.zeros(dims, np.complex_)
        for i in range(nlayer):
            kz[i,:] =  -k0 * np.sqrt(sin_a**2 - self.layers[i].one_n2)

        # iterate from bottom to top
        for i in range(nlayer-2, -1, -1):
            with np.errstate(divide='ignore'):
                pij = (kz[i] + kz[i+1])/(2.* kz[i])
                mij = (kz[i] - kz[i+1])/(2.* kz[i])
            pij[~np.isfinite(pij)] = 0
            mij[~np.isfinite(mij)] = 0

            z = self.layers[i].zval
            exp_p = np.exp(-1j*(kz[i+1]+kz[i])*z)
            exp_m = np.exp(-1j*(kz[i+1]-kz[i])*z)
            a00 = pij * exp_m
            a01 = mij * np.conjugate(exp_p)
            a10 = mij * exp_p
            a11 = pij * np.conjugate(exp_m)
            T[i,:] = a00 * T[i+1] + a01 * R[i+1]
            R[i,:] = a10 * T[i+1] + a11 * R[i+1]
        # normalize
        t0 = T[0].copy()
        t0[t0==0] = 1
        for i in range(nlayer):
            with np.errstate(divide='ignore'):
                T[i,:] /= t0
                R[i,:] /= t0
        T[~np.isfinite(T)] = 0
        R[~np.isfinite(R)] = 0
        return T[order], R[order] 

    def propagation_coeffs(self, alphai, alpha, k0, order):
        Ti, Ri = self.parratt_recursion(alphai, k0, order)
        Tf, Rf = self.parratt_recursion(alpha, k0, order)
        fc = np.zeros((4,) + alpha.shape, np.complex_)
        
        fc[0,:] = Ti * Tf
        fc[1,:] = Ri * Tf
        fc[2,:] = Ti * Rf
        fc[3,:] = Ri * Rf
        fc[:,alpha < 0] = 0
        return fc
        
        
def reflectivity(alpha, delta, beta):
    dns2 = 2 * complex(delta, beta)
    kz = np.sin(alpha)
    kt = np.sqrt(np.sin(alpha)**2 - dns2)
    Rf = -(kz - kt)/(kz + kt)
    return Rf

def propagation_coeffs(alphai, alpha, delta, beta):
    fc = np.zeros((4,) + alpha.shape, np.complex_)
    Ri = reflectivity(alphai, delta, beta)
    Rf = reflectivity(alpha, delta, beta)
    fc[0,:] = 1.
    fc[1,:] = Ri
    fc[2,:] = Rf
    fc[3,:] = Ri * Rf
    fc[:, alpha < 0] = 0
    return fc


if __name__ == '__main__':
    nz = 600
    alphai = np.deg2rad(0.3)
    alpha = np.linspace(-alphai, 0.0145, nz)
    delta = 4.88E-06 
    beta =  7.37E-08 
    
    system = MultiLayer()
    k0 = 2 * np.pi / 0.123984
    fc = system.propagation_coeffs(alphai, alpha, k0, 0)
    f1 = propagation_coeffs(alphai, alpha, delta, beta)

    import matplotlib.pyplot as plt
    ax = plt.subplot(221)
    ax.plot(alpha, np.abs(fc[0,:]), label='EFI')
    ax.plot(alpha, np.abs(f1[0,:]), label='other')
    ax.legend()
    plt.subplot(222)
    plt.plot(alpha, np.abs(fc[1,:]))
    plt.plot(alpha, np.abs(f1[1,:]))
    plt.subplot(223)
    plt.plot(alpha, np.abs(fc[2,:]))
    plt.plot(alpha, np.abs(f1[2,:]))
    plt.subplot(224)
    plt.plot(alpha, np.abs(fc[3,:]))
    plt.plot(alpha, np.abs(f1[3,:]))
    plt.show()
