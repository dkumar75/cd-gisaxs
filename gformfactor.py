#! /usr/bin/env python

import math
import numpy as np
import arrayfire as af
import ipdb

def trapezoid_form_factor(qy, qz, y1, y2, langle, rangle, h):
    m1 = math.tan(langle)
    m2 = math.tan(math.pi - rangle)
    t1 = qy + m1 * qz
    t2 = qy + m2 * qz

    t3 = m1 * af.exp(-1j * qy * y1) * (1 - af.exp(-1j * h / m1 * t1)) / t1
    t4 = m2 * af.exp(-1j * qy * y2) * (1 - af.exp(-1j * h / m2 * t2)) / t2
    ff = (t4 - t3) / qy
    return ff


def stacked_trapezoids(qy, qz, y1, y2, height, langle, rangle=None):
    if not isinstance(langle, np.ndarray):
        raise TypeError('anlges should be an numpy.ndrray')
    if rangle is not None:
        if not isinstance(rangle, np.ndarray):
            raise TypeError('anlges should be an numpy.ndarray')
        if not langle.size == rangle.size:
            raise ValueError('both angle array are not of same size')
    else:
        rangle = langle
    ff = af.constant(0, *qz.dims(), dtype=af.Dtype.c32)

    # loop over all the angles
    shift = 0
    for left, right in zip(langle, rangle):
        ff += trapezoid_form_factor(qy, qz, y1, y2, left, right, height) * af.exp(-1j * shift * qz)

        # update y's and m's for next trapezoid
        shift += height
        m1 = math.tan(left)
        m2 = math.tan(np.pi - right)
        y1 += height / m1
        y2 += height / m2 
    return ff

if __name__ == '__main__':
    alpha = np.linspace(0, 0.2, 1000) 
    wavelen = 0.123984
    k0 = 2 * np.pi / wavelen
    print(k0)
    qz = af.np_to_af_array(k0 * np.sin(alpha))
    qp = af.constant(0.125, *qz.dims())
    langle = np.deg2rad(np.repeat(80, 5))
    rangle = np.deg2rad(np.repeat(85, 5))

    ff = stacked_trapezoids(qp, qz, -25, 25, 10, langle, rangle=rangle)

    qz = qz.__array__()
    ff = ff.__array__()
    import matplotlib.pyplot as plt
    plt.semilogy(alpha, np.abs(ff)**2)
    plt.show() 
