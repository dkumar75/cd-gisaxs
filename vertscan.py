#! /usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
from dwba import SimGISXAS
import time

class ExpSetup:
    """
    converts pixels into qspace.

    Attributes:
    ----------
    center: float, pixels 
        location of the direct beam in w.r.t dectector in pixels
    sdd: float, meters
        sample to detector distance
    pixel_size: float, micrometers
        size of detector pixels
    wavelen: float, nanometer
        wavelength of the incoming planar electromagnetic wave
    det_shape: tuple of ints
        Number of pixels in detector [rows, cols]
    """
    def __init__(self, center, sdd, pixel_size=(172, 172), wavelen=0.123984185, det_shape=(981, 1043)):
        self.center = list(center)
        self.center[1] = det_shape[1] - center[1]
        # convert sdd to microns (same as pixel size units)
        self.sdd = sdd * 1.0E06
        self.pixel_size = pixel_size
        self.wavelen = wavelen
        self.det_shape = det_shape 
        self.k0 = 2*np.pi/self.wavelen

    def angles(self, ypos):
        if ypos is None:
            ypos = self.center[0]

        dy = (ypos - self.center[0]) * self.pixel_size[0]
        tan_th = dy/self.sdd
        sin_th = self.tan2sin(tan_th)
        dz = (np.arange(self.det_shape[0])-self.center[1]) * self.pixel_size[1]
        tan_al = dz/np.sqrt(dy**2 + self.sdd**2)
        sin_al = self.tan2sin(tan_al)
        return sin_th, sin_al

    @staticmethod
    def tan2sin(tan_a):
        t = tan_a**2
        return np.sign(tan_a) * np.sqrt(t/(1+t))

def vertical_scan(ypos, alphai, exp, params):
    n_ai = len(alphai)
    sin_th, sin_al = exp.angles(ypos)
    cos_al = np.sqrt(1.- sin_al**2)
    k0 = exp.k0
    qy = k0 * cos_al * sin_th
    qy = np.repeat(qy[:,np.newaxis], n_ai, axis=1)
    sin_ai = np.sin(alphai)
    qz = k0 * np.add.outer(sin_al, sin_ai)
    sim = SimGISXAS(qy, qz, exp.wavelen, refindx)
    ff = sim.run(params, alphai)
    ff[qz<=0] = np.complex(0, 0)
    ff = np.abs(ff)**2
    return np.sum(ff,axis=1)

if __name__ == '__main__':
    # setup experiment
    center = (741, 1353)
    sdd = 0.834
    wave = 0.4959
    det = (1679, 1475)
    ypos = 771
    exp = ExpSetup(center, sdd, wavelen=wave, det_shape=det)

    # shape
    params = { 'left_coord':-25, 'right_coord':25, 'delta_h':10 }
    params['langle'] = np.deg2rad(np.repeat(80, 5))

    # refrective index
    refindx = (4.88E-06, 7.37E-08)

    # beam wavelength
    wavelen = 0.123984
    alphai = np.deg2rad(np.linspace(0.15, 3., 300))
   
    import matplotlib.pyplot as plt
    t0 = time.time()
    I = vertical_scan(ypos, alphai, exp, params)
    I[I < 1] = 1
    dt =  time.time() - t0
    print('Time take = %f' % dt)
    X = np.arange(det[0])
    plt.semilogy(X, I[X])
    plt.show()
